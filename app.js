// Initial map
var map = L.map('mapid', {
    center: [51.505, -0.09], // London
    zoom: 13
});

// For LeafletEasybutton instance
var selectfeature = null;

// Set zoom button to bottom right corner of the map
map.zoomControl.setPosition('bottomright');

// Set tiles from OpenStreetMap
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png?{foo}', {
    foo: 'bar',
    attribution: '&copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a>'
}).addTo(map);

// Event when finished drawing
map.on('selectAreaFeature:mouseUp', function ({event, area}) {
    map.fitBounds(area.getBounds());
});

// Event when click on polygon
map.on('selectAreaFeature:area:click', function (e) {
    if (!selectfeature.enabled()) {
        selectfeature.removeAreaByIndex(e.sourceTarget._leaflet_id);
    }
});

// Add button to enable drawing
L.easyButton('fa-pen', function (btn) {
    selectfeature = map.selectAreaFeature.enable();
    selectfeature.options.delete_color = '#d1482a';
    selectfeature.options.draw_color = '#3eacb9';
    selectfeature.options.dashArray = '';
}, 'Enable drawing', 'map-enable-draw').addTo(map);

// Add button to disable drawing
L.easyButton('fa-trash', function () {
    if (selectfeature !== null) {
        selectfeature.disable();
    }
}, 'Disable drawing', 'map-disable-draw').addTo(map);

// Add button for help instruction
L.easyButton('fa-question', function () {
    console.log('Help instruction');
}, 'Instructions', 'map-help').addTo(map);
