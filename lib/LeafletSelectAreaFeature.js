(function (factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD
        define(['leaflet'], factory);
    } else if (typeof module !== 'undefined') {
        // Node/CommonJS
        module.exports = factory(require('leaflet'));
    } else {
        // Browser globals
        if (typeof window.L === 'undefined') {
            throw new Error('Leaflet must be loaded first');
        }
        factory(window.L);
    }
}(function (L) {
    "use strict";
    L.SelectAreaFeature = L.Handler.extend({

        options: {
            draw_color: 'blue',
            delete_color: 'red',
            weight: 2,
            dashArray: '5, 5, 1, 5' ,
            selCursor: 'crosshair',
            normCursor: ''
        },

        initialize: function (map, options) {
            this._map = map;

            this._pre_latlon = '';
            this._post_latlon = '';
            this._ARR_latlon_line = [];
            this._ARR_latlon = [];

            this._area_pologon_layers = [];

            this._area_line = '';
            this._area_line_new = '';

            L.setOptions(this, options);
        },

        addHooks: function() {

            this._map.on('mousedown', this._doMouseDown, this );
            this._map.on('mouseup', this._doMouseUp, this );

            this._map.dragging.disable();

            this._map._container.style.cursor = this.options.selCursor;

            this._reloadAllArea(this.options.draw_color);
        },

        removeHooks: function() {
            this._map.off('mousemove', this._doMouseMove, this );
            this._map.off('mousedown', this._doMouseDown, this );
            this._map.off('mouseup', this._doMouseUp, this );
            this._map._container.style.cursor = this.options.normCursor;

            this._map.dragging.enable();

            this._reloadAllArea(this.options.delete_color);
        },

        _doMouseUp: function(ev) {

            if (this._ARR_latlon.length < 1) {
                this._map.off('mousemove');
                return;
            }

            this._pre_latlon = '';
            this._post_latlon = '';
            this._ARR_latlon_line = [];

            const _polygon = L.polygon(this._ARR_latlon, {
                color: this.options.draw_color,
                fillColor: this.options.draw_color,
                fillOpacity: 0.2
            });

            this._area_pologon_layers.push(_polygon.addTo(this._map));

            _polygon.on('mouseover', function () {
                _polygon.setStyle({ fillOpacity: 0.3 })
            });

            _polygon.on('mouseout', function () {
                _polygon.setStyle({ fillOpacity: 0.2 })
            });

            _polygon.on('click', function (e) {
                this._map.fireEvent('selectAreaFeature:area:click', e);
            });

            if ( this._map.hasLayer(this._area_line) ){
                this._map.removeLayer(this._area_line);
            }
            if ( this._map.hasLayer(this._area_line_new) ){
                this._map.removeLayer(this._area_line_new);
            }
            this._map.off('mousemove');
            this._map.fireEvent('selectAreaFeature:mouseUp', {event: ev, area: _polygon});

        },

        _doMouseDown: function(ev) {

            this._ARR_latlon = [];
            this._area_pologon = '';
            this._area_line_new = '';
            this._area_line = '';

            this._map.on('mousemove', this._doMouseMove, this );
        },

        _doMouseMove: function(ev) {

            this._ARR_latlon.push(ev.latlng);
            if (this._pre_latlon == '' || this._pre_latlon == "undefined") {
                this._pre_latlon = ev.latlng;
                this._ARR_latlon_line.push(this._pre_latlon);
            }
            else if ( this._pre_latlon != '' && ( this._post_latlon == '' || this._post_latlon == "undefined") ) {
                this._post_latlon = ev.latlng;
                this._ARR_latlon_line.push(this._post_latlon);
            }
            else {
                this._pre_latlon = this._post_latlon;
                this._post_latlon = ev.latlng;
                this._ARR_latlon_line.push(this._pre_latlon);
                this._ARR_latlon_line.push(this._post_latlon);
            }

            if ( this._pre_latlon != '' && this._post_latlon != '' ) {
                if ( this._area_line_new == '' && this._area_line == '' ) {
                    this._area_line = L.polyline(this._ARR_latlon_line, {
                        color: this.options.draw_color,
                        weight: this.options.weight,
                        dashArray: this.options.dashArray
                    });

                    this._area_line.addTo(this._map);
                }
                if ( this._area_line_new == '' && this._area_line != '' ) {
                    this._area_line_new = L.polyline(this._ARR_latlon_line, {
                        color: this.options.draw_color,
                        weight: this.options.weight,
                        dashArray: this.options.dashArray
                    });

                    this._area_line_new.addTo(this._map);
                    this._map.removeLayer(this._area_line);
                }
                if ( this._area_line_new != '' && this._area_line != '' ) {
                    this._area_line = L.polyline(this._ARR_latlon_line, {
                        color: this.options.draw_color,
                        weight: this.options.weight,
                        dashArray: this.options.dashArray
                    });
                    this._area_line.addTo(this._map);
                    this._map.removeLayer(this._area_line_new);
                    this._area_line_new = '';
                }

            }

        },

        _reloadAllArea: function(color) {
            var _i = 0;

            while ( _i < this._area_pologon_layers.length  ) {
                this._area_pologon_layers[_i].setStyle({ fillColor: color });
                _i++;
            }

            this._map.eachLayer(function(layer) {
                if (layer instanceof L.Polyline) {
                    layer.setStyle({ color: color });
                }
            });
        },

        _isMarkerInsidePolygon: function (marker, poly) {
            const polyPoints = poly.getLatLngs()[0];
            const x = marker.getLatLng().lat;
            const y = marker.getLatLng().lng;
            var inside = false;
            for (var i = 0, j = polyPoints.length - 1; i < polyPoints.length; j = i++) {
                const xi = polyPoints[i].lat;
                const yi = polyPoints[i].lng;
                const xj = polyPoints[j].lat;
                const yj = polyPoints[j].lng;
                var intersect = ((yi > y) !== (yj > y))
                    && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
                if (intersect) {
                    inside = !inside;
                }
            }
            return inside;
        },

        getAreaLatLng: function() {
            return this._ARR_latlon;
        },

        removeAllArea: function() {
            var _i = 0;
            while ( _i < this._area_pologon_layers.length  ) {
                this._map.removeLayer(this._area_pologon_layers[_i]);
                _i++;
            }
            this._area_pologon_layers.splice( 0, _i );
        },

        removeAreaByIndex: function(layer_id) {
            const area_index = this._area_pologon_layers.findIndex(function (area) {
                return area._leaflet_id === layer_id;
            });

            if (area_index === -1) {
                return;
            }

            this._map.removeLayer(this._area_pologon_layers[area_index]);
            this._area_pologon_layers.splice(area_index, 1);
        },

        removeLastArea: function() {
            var index = this._area_pologon_layers.length - 1;
            this._map.removeLayer(this._area_pologon_layers[index]);
            this._area_pologon_layers.splice(index, 1);
        },

        getFeaturesSelected: function(layertype) {
            var layers_found = [];
            var pol;
            var _i = 0;

            while ( _i < this._area_pologon_layers.length  ) {
                pol = this._area_pologon_layers[_i].getBounds();

                this._map.eachLayer(function(layer){
                    if ( (layertype == 'polygon' || layertype == 'all') && layer instanceof L.Polygon && !pol.equals(layer.getBounds()) ) {
                        if ( pol.contains(layer.getBounds()) ) {
                            layers_found.push(layer);
                        }
                    }
                    if ( (layertype == 'polyline' || layertype == 'all') && layer instanceof L.Polyline && !pol.equals(layer.getBounds()) ) {
                        if (  pol.contains(layer.getBounds()) ) {
                            layers_found.push(layer);
                        }
                    }
                    if ( (layertype == 'circle' || layertype == 'all') && layer instanceof L.Circle && !pol.equals(layer.getBounds()) ) {
                        if ( pol.contains(layer.getBounds()) ) {
                            layers_found.push(layer);
                        }
                    }
                    if ( (layertype == 'rectangle' || layertype == 'all') && layer instanceof L.Rectangle && !pol.equals( layer.getBounds()) ) {
                        if ( pol.contains(layer.getBounds()) ) {
                            layers_found.push(layer);
                        }
                    }
                    if ( (layertype == 'marker' || layertype == 'all') && layer instanceof L.Marker  ) {
                        if ( pol.contains(layer.getLatLng()) ) {
                            layers_found.push(layer);
                        }
                    }
                    if ( (layertype == 'circlemarker' || layertype == 'all') && layer instanceof L.CircleMarker  ) {
                        if ( pol.contains(layer.getLatLng()) ) {
                            layers_found.push(layer);
                        }
                    }
                });
                _i++;
            }
            if ( layers_found.length == 0  ){
                layers_found = null;
            }

            return layers_found;
        }

    });

}, window));

L.Map.addInitHook('addHandler', 'selectAreaFeature', L.SelectAreaFeature);
